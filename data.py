#! /usr/bin/python3

import pandas as pd
import numpy as np
import seaborn as sb
import matplotlib.pyplot as plt
print("lib loaded")

#DOC : https://seaborn.pydata.org/generated/seaborn.relplot.html
#DOC : https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.to_datetime.html

house = pd.read_csv("../data/house_pricing.csv")
print(house)
print(house.describe)
print(house.dtypes)

sb.set()
sb.relplot(x="SalePrice", y="LotArea", data=house)

house = house.loc[house['LotArea'] > 20000] 
house = house.loc[house['SalePrice'] < 500000] 
sb.relplot(x="SalePrice", y="LotArea", data=house)

house = house.loc[house['LotArea'] < 100000] 
house = house.loc[house['LotFrontage'] < 200] 
sb.relplot(x="SalePrice", y="LotArea", data=house)

plt.style.use("dark_background")
plt.show() #Pour afficher le graph sous Widows, il faut le lancer depuis la CMD Windows et non WLS
#plt.savefig('graph.png')