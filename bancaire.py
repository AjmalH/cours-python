#! /usr/bin/python3

class CompteBancaire:
    def __init__(self, nom="Dupont", solde=1000):
        self.solde = solde
        self.nom = nom

    def depot(self, montant):
        self.solde += montant

    def retrait(self, montant):
        self.solde -= montant

    def __str__(self):
        return self.solde

    def affiche(self):
        print("Le solde du compte bancaire de {} est de {} euros.".format(self.nom, self.solde))



compte1 = CompteBancaire('Duchmol', 800)
compte1.depot(350)
compte1.retrait(200)
compte1.affiche()


compte2 = CompteBancaire()
compte2.depot(25)
compte2.affiche()