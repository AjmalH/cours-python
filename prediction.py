#! /usr/bin/python3

import pandas as pd
import numpy as np
import seaborn as sb
import matplotlib.pyplot as plt
print("lib loaded")


prediction = pd.read_csv("../data/sales_predictions.csv")
print(prediction)
print(prediction.describe)
print(prediction.dtypes)

#Commenté pour cause de lag
#sb.relplot(x="date", y="item_price", data=prediction)


prediction = prediction.sample(50)
print(prediction)

sb.set()
sb.relplot(x="date", y="item_price", data=prediction)
sb.relplot(x="date", y="item_price", sizes=(20, 6), data=prediction)

prediction["date"] = pd.to_datetime(prediction["date"])
sb.relplot(x="date", y="item_price", sizes=(20, 6), data=prediction)
print(prediction)

plt.show() #Pour afficher le graph sous Widows, il faut le lancer depuis la CMD Windows et non WLS