#! /usr/bin/python3

class DateNaissance:
    def __init__(self, jour, mois, annee):
        self.jour = jour
        self.mois = mois
        self.annee = annee

    def ToString(self):
        return "{}/{}/{}".format(self.jour, self.mois, self.annee)
        

class Personne():
    def __init__(self, nom, prenom, dn):
        self.nom = nom
        self.prenom = prenom
        self.dn = dn

    def afficher(self):
        print("Nom : " + self.nom)
        print("Prénom : " + self.prenom)
        print("Date de naissance : " + self.dn.ToString())

class Employe(Personne):
    def __init__(self, nom, prenom, dn, salaire):
        super().__init__(nom, prenom, dn)
        self.salaire = salaire
    
    def afficher(self):
        super().afficher()
        print("Salaire : {}".format(self.salaire))

class Chef(Employe):
    def __init__(self, nom, prenom, dn, salaire, service):
        super().__init__(nom, prenom, dn, salaire)
        self.service = service
    
    def afficher(self):
        super().afficher()
        print("service : {}".format(self.service))




P=Personne("Ilyass","Math",DateNaissance(1,7,1982))
P.afficher()
print("========")
E=Employe("Ilyass","Math",DateNaissance(1,7,1985),7865.548)
E.afficher()
print("========")
Ch=Chef("Ilyass","Math",DateNaissance(1,7,1988),7865.548,"Ressource humaine")
Ch.afficher()