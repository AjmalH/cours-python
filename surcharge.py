#! /usr/bin/python3

class Point:
    def __init__(self, x, y, z=None):
        self.x = x
        self.y = y
        self.z = z

    def ToString(self):
        if self.z is not None:
            print("P({} , {}, {})".format(self.x, self.y, self.z))
        else:
            print("P({} , {})".format(self.x, self.y))
        

    def ToString2(self):
        print( ("P({} , {}, {})".format(self.x, self.y, self.z)) if self.z is not None else ("P({} , {})".format(self.x, self.y)) )

P1=Point(2,3)
P1.ToString()
P2=Point(1,-5,6)
P2.ToString()
